import React from 'react';
import './css/App.css';
import { Select, OptionType } from './Components/Select';

const options: OptionType[] = [
  'Airport',
  'Alternative',
  'Billboard',
  'Retail',
  'Sports',
  'Venues',
  'Street',
  'Furniture',
  'Transit',
  'Wallscape',
  'Wildposting'
];

const App: React.FC = () => {

  const initialValues: OptionType[] = [];
  const [selectedValues, setSelectedValues] = React.useState(initialValues);

  return (
    <div className="App">
      <pre>{selectedValues.join(', ')}</pre><br />
      <Select
        selected={selectedValues}
        options={options}
        onChange={(selected: OptionType[]) => setSelectedValues(selected)} />
    </div>
  );
}

export default App;
