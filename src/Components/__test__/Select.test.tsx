import React from 'react';
import { Select } from '../Select';
import { ReactWrapper, mount } from 'enzyme';

describe('Select Component', () => {

	let select: ReactWrapper;

	beforeEach(() => select = mount(<Select options={['Foo', 'Bar']} />))

	it('should render without crashing', () => {
		expect(select).toMatchSnapshot();
	});

	it('should render options', () => {
		expect(select.find('.option')).toHaveLength(2);
	});

	it('should update options when props changes', () => {

		expect(select.find('.option').map(opt => opt.text()))
			.toMatchObject(['Foo', 'Bar']);

		select.setProps({ options: ['test1', 'test2', 'test3'] });
		select.update();

		expect(select.find('.option').map(opt => opt.text()))
			.toMatchObject(['test1', 'test2', 'test3']);
	});

	it('should toggle options by clicking in the button', () => {
		expect(select.find('.options').hasClass('open')).toBe(false);
		select.find('button').simulate('click');
		expect(select.find('.options').hasClass('open')).toBe(true);
		select.find('button').simulate('click');
		expect(select.find('.options').hasClass('open')).toBe(false);
	});

	it('should change label accordingly to selected options count', () => {
		select.find('button').simulate('click');
		expect(select.find('button').text()).toBe('Media Type');

		select.find('.option').at(1).simulate('click');
		expect(select.find('button').text()).toBe('Bar only');

		select.find('.option').at(0).simulate('click');
		expect(select.find('button').text()).toBe('Media Types • 2');
	});
	
	it('should set active style for selected options', () => {
		select.find('button').simulate('click');
	
		expect(select.find('.option').at(1).hasClass('active')).toBe(false);
		select.find('.option').at(1).simulate('click');
		expect(select.find('.option').at(1).hasClass('active')).toBe(true);
	});
});
