import React from 'react';
import '../css/Select.css';
import { FaCheck } from 'react-icons/fa';

export type OptionType = string;

export interface SelectProps {
	options: OptionType[],
	onChange?: (selected: OptionType[]) => any,
	selected?: OptionType[]
}

export const Select = (props: SelectProps): React.ReactElement => {

	const DEFAULT_LABEL = 'Media Type';
	const initialValue: OptionType[] = props.selected || [];

	const [selected, setSelected] = React.useState(initialValue);
	const [label, setLabel] = React.useState(DEFAULT_LABEL);
	const [options, setOptions] = React.useState(props.options);
	const [optionsOpen, setOptionsOpen] = React.useState(false);

	const isOptionActive = (option: OptionType): boolean => selected.includes(option);

	const filterOptions = (option: OptionType): OptionType[] =>
		selected.filter((selectedOption: OptionType) => selectedOption !== option)

	const toggleSelected = (option: OptionType) => {
		setSelected(isOptionActive(option) ? filterOptions(option) : [...selected, option]);
	}

	React.useEffect(() => {
		if (selected.length === 1) {
			setLabel(`${selected[0]} only`);
		}
		if (selected.length > 1) {
			setLabel(`Media Types • ${selected.length}`);
		}
		if (selected.length < 1) {
			setLabel(DEFAULT_LABEL);
		}
		props.onChange && props.onChange(selected);
	}, [selected, props]);

	React.useEffect(() => {
		setOptions(props.options);
	}, [props.options]);

	return (
		<div className="select">
			<button
				onClick={() => setOptionsOpen(!optionsOpen)}
				className={`${(selected.length || optionsOpen) && 'active'}`}>
				{label}
			</button>
			<ul className={`options ${optionsOpen && 'open'}`}>
				{options.map((option, i) =>
					<li
						key={`opt_${i}`}
						onClick={() => toggleSelected(option)}
						className={`option ${isOptionActive(option) && 'active'}`}>
						{option}
						<FaCheck className={`icon ${isOptionActive(option) && 'show'}`} size="12px" />
					</li>
				)}
			</ul>
		</div>
	);
}