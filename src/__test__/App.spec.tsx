import React from 'react';
import App from '../App';
import { shallow } from 'enzyme';

describe('Main App', () => {

  it('should render without crashing', () => {
    const app = shallow(<App />);
    expect(app).toMatchSnapshot();
  });
});
